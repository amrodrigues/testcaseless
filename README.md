Testcaseless
============

Testcaseless is an experiment in API testing targeting web APIs. Structural mutations or value mutations on a graph of JSON objects are monitored to detect errors.

As the name suggests, the core idea is to achieve productivity gains by eliminating the need of creating and maintaining test cases. The goal is to make testcaseless intelligent enough so that errors are identified almost without human intervention.

The second core idea is that, ideally, we should not have configuration parameters in the sense of a configuration file.

Value mutations, e.g. value 5 in {"times": 5} can be configured as ranges, regex patterns or sets. An unexpected value found for the first time, e.g. {"times": 7} will trigger a user prompt to classify this as an error, edit value criteria or add as a new acceptable observation.


### Run modes

1. Listen

     In this mode, Testcaseless acts as a web proxy intercepting anything you call in your configured browser. In this discovery mode, Testcaseless learns the initial URLs that should be stored.

2. Replay

     In "replay" mode, Testcaseless will reproduce all historical API requests and log anything strange it finds in the responses. It should be able to discover new URLs and crawl them as well (if your API is a hypermedia API, i.e. contains URLs).

3. Monitor

     This mode will allow continuous testing by replaying history continuously against a target domain.


Testcaseless is a good excuse to play with Go language, Cassandra and with a different kind of software testing that is neither functional testing nor unit testing.

### Version 0.2

Testcaseless is already smart enough to store GET and POST requests and responses. It has started detecting changes in historical variants but currently is undegoing a heavy redesign to make it clever on value analysis.


### Installation

1. Install Cassandra v2 (not v3!). This is just a file you download and unpack somewhere in your user folder. Go to https://cassandra.apache.org/ and download Cassandra version 2 for your OS (not version 3).

    Edit your PATH environment variable and add Cassandra's /bin directory there so that 'cassandra' and 'cqlsh' commands are available anywhere in your system. Restart your terminal and enter 'cassandra' command to launch Cassandra.

2. Run conf/createdb.bash script to create the database.

3. In your web browser go to the advanced network definitions and proxy requests to localhost on port 8080.

4. Open up a terminal and run testcaseless. It will intercept your HTTP requests while you browse your app.


### Theory

As software changes over time the web API will mutate. The basis for error detection is signaling changes either in the hypermedia API graph or in the set of REST API endpoints.

### Mutation levels

1. **Graph**: This is a top level mutation in which the graph itself changes (eg. a URL is added or removed in a JSON document linking to other JSON documents).

2. **Document**: A structural change happens in a JSON document, for example one of its object contains a new key, or an array value is replaced with a String value.

3. **Data**: An atomic change within a previously seen JSON structure. A data change happens, for example values in a String, Array or Number.


### Change classifiers

The change itself can be of the following types:

1. **Variant**: the occurrence is seen as part of a set of acceptable alternatives. The problem is to assess if a change is only acceptable if it matches the alternatives on sets of other changes in the same document.

    Variant evaluation of a change can consider scalars, string literals, number value ranges or string patterns.

2. **Error**: the occurrence has been seen before and was classified by manual or automated means as an error.

3. **Unknown**: the occurrence happens for the first time, therefore there is no knowledge of it


### Licence
[MIT](LICENSE)