#!/usr/bin/env bash

if ! [ -x "$(command -v cqlsh)" ]; then

  echo "Could not create database."
  echo "If you don't have Apache Cassandra installed and running, it's just a file you download and unpack. Go to https://cassandra.apache.org/ and download Cassandra version 2 for your OS (not version 3)."
  echo "Edit your PATH environment variables and add Cassandra's /bin directory so that 'cassandra' and 'cqlsh' commands are available anywhere in your system."
  echo "Execute cassandra command to launch Cassandra."
  echo "Then run this script to create the database."

  exit 1

fi

cqlsh -f schema.cql