package main

import (
	"os"
	"gitlab.com/amrodrigues/testcaseless/caseless/services"
	"gitlab.com/amrodrigues/testcaseless/caseless/storage"
	"gitlab.com/amrodrigues/testcaseless/caseless"
)

func main() {

	println("\n\n\n\n\n\n\n\n\n\n\n\n\n")
	println("┌┬┐┌─┐┌─┐┌┬┐┌─┐┌─┐┌─┐┌─┐┬  ┌─┐┌─┐┌─┐")
	println(" │ ├┤ └─┐ │ │  ├─┤└─┐├┤ │  ├┤ └─┐└─┐")
	println(" ┴ └─┘└─┘ ┴ └─┘┴ ┴└─┘└─┘┴─┘└─┘└─┘└─┘")
	println("")

	var args = os.Args
	var mode string

	if len(os.Args) < 2 {

		mode = "listen"
		println("Defaulting to \"listen\" mode.")
		println("Other modes are: \"replay\" and \"spider\". Example: testcaseless replay")

	} else {

		mode = args[1]
		if mode == "listen" {
			println("Starting \"listen\" mode.")
		}
	}

	switch mode {

	case "listen":

		var tc = new(services.Listen)

		store := new(storage.Store)
		store.Connect()

		history := new(caseless.History)
		history.SetStore(store)

		tc.Listen(*store, *history)

	case "replay":

		var tc = new(services.Replay)
		tc.Replay()

	case "spider":

		//todo
	}

}
