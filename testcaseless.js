"use strict";

// this file should be injected in html pages by testcaseless proxy via:
// <script src="/testcaseless/testcaseless.js" type="text/javascript" />

//todo replay mode
// a) getting an ordered sequence of past get+post urls [{"url": "/xxx/xxx", "method": "post", "ts": "TS"}, {...}]
// b) iterating array of requests; if the method is POST, get post body from proxy ("virtual" url) and submit it
// c) a listening instance of testcaseless should give feedback on diffs as usual