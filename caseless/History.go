package caseless

import (

	"gitlab.com/amrodrigues/testcaseless/caseless/storage"
)

// process a Node against its history

type History struct {

	store *storage.Store
	diff  *Diff
	app   string
}

func (h *History) SetStore(s *storage.Store) {

	h.store = s
	h.diff = new(Diff)
	h.diff.SetStore(s)

	h.app = "default"
}

func (h *History) DiffContainsNodes() bool {
	return h.diff.containsNodes()
}


func (h *History) DiffClassifyNext() {
	h.diff.classifyNext()
}

// process the Body of a request or Node
// this will decide if current request/response goes in the database

func (h *History) Compare(n *Node) {

	if n == nil {
		println("[error] processBody: Node is nil")
	}

	// If an exact Body exists in the database
	// flag error if it's classified an error, otherwise ignore it

	history := h.store.FetchHistory(n)

	// case: new Node
	h.ProcessHistory(n, history)

	foundHistory := n.Compared != nil

	// case: new node
	// options here would be: consider first instance as non error or prompt user for Change

	if len(history) == 0 || !foundHistory {

		// avoid displaying twice:
		if n.Re == "response" {

			println("_______ _____  ______ _______ _______      _______ _______ _______ __   _")
			println("|______   |   |_____/ |______    |         |______ |______ |______ | \\  |")
			println("|       __|__ |     \\_ _____|    |         ______| |______ |______ |  \\_|")

			println("[warn]", "First seen:", n.Url)
		}

		answer := h.diff.getInput("Adding as First Seen. OK ? (y/n)")

		if answer == "y" {

			h.store.Insert(n)
			println("Inserted.")

		} else {
			println("Ignoring.")
		}

		return
	}

	// case: history found for this Node

	h.ProcessHistory(n, history)
}


func (h *History) ProcessHistory(n *Node, history []*Node) {

	println("Filtered Body comparison...")

	for ix := range history {

		if len(history[ix].FilteredBody) == len(n.FilteredBody) && history[ix].FilteredBody == n.FilteredBody {

			// case: match was classified as error

			if history[ix].Change == "error" {

				h.diff.print(n)
				AsciiError()
				println("[ERROR]", n.Method, n.Url)
			}

			// case: found match but it's not an error
			return
		}
	}

	// case: no filtered match exists, deliver for Diff processing

}



// test if a request or response Body is equivalent after running stripping out UUIDS, dates, etc.

func (h *History) IsEquivalent(responseBody string, storedResponseBody string) bool {

	//TODO

	return h.diff.globalFilter(responseBody) == h.diff.globalFilter(storedResponseBody)
}

func (h *History) Stop() {
	h.store.Session.Close()
}
