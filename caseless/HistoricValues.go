package caseless


// Collect all historic values for each path
// This allows to derive patterns (semi-)automatically
// Example: if historic values for a Json path are 4,2,5,1,0, Testcaseless can ask if the
// range of acceptable values is 0-5

type HistoricValues struct {

	values map[string][]string
}


// iterate all Json paths on all historic nodes and collect valid json values
// a historic node might be tagged as error, the function should consider that

func (s *HistoricValues) Collect (n *Node, history []Node) {

	if len(history) == 0 {
		return
	}

	// iterate history

	for _, historyNode := range history {
		//todo
		historyPaths := historyNode.Flattener.AsMap()

		for k, v := range historyPaths {

			// check if slice exists:

			if s.values[k] == nil {
				s.values[k] = make([]string, 10)
			}

		}
	}


}
