package caseless


// A graph Node representing a request or a response

type Node struct {

	Url           string              // relative Url
	Ts            string              // Node timestamp
	Method        string              // "get" or "post"
	Re            string              // "request" or "response"
	Body          string              // Node Body
	Bytes         []byte              // Node Body as []byte
	FilteredBody  string              // Body after global filters
	FilteredBytes []byte              // Body after filtering out patterns
	Change        string              // type of Change at JSON document level: "variant", "error", "unknown"
	Compared      *Node               // a different version found of this Node
	Diff          string              // json Diff of current Node with Compared Node
	Cookies       string              // HTTP Cookies
	ContentType   string              // json, html, etc.
	Flattener     *Flattener // json body as mapping of paths to values | ranges | set (if body is json)
	HistoricValues *HistoricValues    // collect historic values for each Json path in Flattener (to derive patterns)
}

func (n *Node) Print() {

	println(" ")
	println("---------")
	println("URL:", n.Url)
	println("Ts:", n.Ts)
	println("Method:", n.Method)
	println("Request/Response:", n.Re)
	println(" ")
}