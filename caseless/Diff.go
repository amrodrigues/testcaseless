package caseless

import (
	"encoding/json"
	"fmt"
	"github.com/yudai/gojsondiff"
	"github.com/yudai/gojsondiff/formatter"
	"os"
	"bufio"
	"strings"
	"github.com/sergi/go-diff/diffmatchpatch"
	"bytes"
	"gitlab.com/amrodrigues/testcaseless/caseless/storage"
	"gitlab.com/amrodrigues/testcaseless/caseless/filters"
)



//TODO deprecate, move relevant functions to filters

type Diff struct {

	store *storage.Store

	diffqueue []*Node // allows the request to be served and only after classifying
	// otherwise the user cannot see the visual result before classifying
}


func (d *Diff) SetStore(s *storage.Store) {
	d.store = s
}


// when: n has no exact equivalent stored in history

func (d *Diff) ProcessDiff(n *Node, history []*Node) {

	//todo implement recursive comparison using a json parser
	//todo add functionality to derive patterns from historic values, e.g. if an integer value mutates 3 times, ask if we can add an acceptable range.
	//todo add option to ignore specific changes in Body on a specific Url
	//todo add option to accept a specific pattern in a specific value

	type MatchFunction func(rune, rune) bool

	type Options struct {
		InsCost int
		DelCost int
		SubCost int
		Matches MatchFunction
	}

	var mostSimilarNode *Node

	globalFilter := new(filters.Global)

	// add filtered versions to Node

	if len(n.FilteredBody) == 0 {
		globalFilter.Run(n)
	}

	n.FilteredBytes = []byte(n.FilteredBody)

	for ix := range history {

		println("Diffing history (", ix, "of", len(history), ")")

		if history[ix].Change == "error" {
			continue
		}

		// add filtered versions to history item

		globalFilter.Run(history[ix])

		if n.FilteredBody == history[ix].FilteredBody {

			mostSimilarNode = history[ix]

			println("Found similar, " + history[ix].Ts)

			break
		}

	}

	// add Node for classification after the response is processed
	// this avoids blocking the response

	n.Compared = &mostSimilarNode
	d.diffqueue = append(d.diffqueue, n)
}


func (d *Diff) getJsonDiffs(n *Node, mostSimilarNode Node) string {

	n.FilteredBytes = []byte(mostSimilarNode.FlatBody)
	mostSimilarNode.FilteredBytes = []byte(mostSimilarNode.FlatBody)

	differ := gojsondiff.New()
	diffs, err := differ.Compare(n.FilteredBytes, mostSimilarNode.FilteredBytes)

	if err != nil {
		fmt.Printf("[error]", "ProcessJsonDiff: could not unmarshal json: %s, %s, %s\n", n.Url, n.Body, err.Error())
		n.Diff = ""
		return ""
	}

	deltaFormatter := formatter.NewDeltaFormatter()

	var diffString, err2 = deltaFormatter.Format(diffs)
	if err2 != nil {
		println("[error]", "ProcessJsonDiff: Error when formatting diffs")
	}

	return diffString
}


func (d *Diff) getHtmlDiffs(n *Node, mostSimilarNode Node) string {

	var dmp = diffmatchpatch.New()
	var diffs = dmp.DiffMain(mostSimilarNode.Body, n.Body, false)

	var r string

	for i := 0; i < len(diffs); i++ {
		r = r + diffs[i].Text + "\n"
	}

	return r
}



func (d *Diff) containsNodes() bool {
	return len(d.diffqueue) > 0
}

func (d *Diff) classifyNext() {

	if len(d.diffqueue) == 0 {
		return
	}

	n := d.diffqueue[0]
	d.classify(n)

	// remove Node already classified
	d.removeFromDiffQueue(n)
}


// display Diff between historical Body and current one just found

func (d *Diff) showDiff(n *Node) {

	println(n.Method, n.Url)

	println("BEFORE +++++++++++++++++++++++++++++++")
	d.print(n)
	println(" ")

	println("DIFF +++++++++++++++++++++++++++++++")
	println(n.Diff)
	println(" ")

	println(n.Method, n.Url)
}

func (d *Diff) print(n *Node) {


	switch n.ContentType {
	case "json":
		 d.printJson(n.Body)
	case "html":
		 d.printHtml(n.Body)
	}
}

func (d *Diff) classify(n *caseless.Node) {

	println("Diff...")

	d.showDiff(n)

	for {

		println("______ _                            _               _  __")
		println("| ___ \\ |                          | |             (_)/ _|")
		println("| |_/ / | ___  __ _ ___  ___    ___| | __ _ ___ ___ _| |_ _   _")
		println("|  __/| |/ _ \\/ _` / __|/ _ \\  / __| |/ _` / __/ __| |  _| | | |")
		println("| |   | |  __/ (_| \\__ \\  __/ | (__| | (_| \\__ \\__ \\ | | | |_| |")
		println("\\_|   |_|\\___|\\__,_|___/\\___|  \\___|_|\\__,_|___/___/_|_|  \\__, |")
        println("                                                           __/ |")
        println("                                                           |___/")
		println("Node: " + n.Url)

		answer := d.getInput("ENTER=variant i=ignore e=error d=delete-stored dd=delete-history")

		switch answer {

		case "e":
			n.Change = "error"
			d.store.Insert(n)
			println("Classified as error.\n\n\n---")
			return

		case "d":
			d.store.Delete(n.Compared)
			d.removeFromDiffQueue(n)
			println("Deleted from database: " + n.Url)
			return

		case "dd":
			d.store.DeleteUrlHistory(n.Compared)
			d.removeFromDiffQueue(n)
			println("Deleted all history of Url\n\n\n---", n.Url)
			return

		case "i":
			d.removeFromDiffQueue(n)
			println("Ignored: " + n.Url)
			return

		case "":
			n.Change = "variant"
			d.store.Insert(n)
			println("Accepted as variant.\n\n\n---")
			return
		}

		println("Pardon?")
	}

}

func (d *Diff) removeFromDiffQueue(n *caseless.Node) {

	if d.diffqueue == nil || len(d.diffqueue) == 0 {
		return
	}

	for ix, item := range d.diffqueue {
		if item == nil {
			break
		}
		if item.Url == n.Url {
			d.removeFromDiffQueueIx(ix)
		}
	}

}

func (d *Diff) removeFromDiffQueueIx(i int) {

	d.diffqueue[i] = d.diffqueue[len(d.diffqueue)-1]
	d.diffqueue[len(d.diffqueue)-1] = nil
	d.diffqueue = d.diffqueue[:len(d.diffqueue)-1]
}

func (d *Diff) getInput(echo string) string {

	if echo != "" {
		println(echo)
	}
	reader := bufio.NewReader(os.Stdin)
	text, _ := reader.ReadString('\n')
	text = strings.Replace(text, "\n", "", -1)
	return text
}

func (d *Diff) jsonDiffOutput(aString []byte, diff gojsondiff.Diff, err error, format string) {

	var diffString string

	if format == "ascii" {

		var aJson map[string]interface{}
		json.Unmarshal(aString, &aJson)
		fmtr := formatter.NewAsciiFormatter(aJson)
		//formatter.ShowArrayIndex = true
		diffString, err = fmtr.Format(diff)
		if err != nil {
			// No error can occur
		}
		fmt.Print(diffString)

	} else if format == "delta" {

		fmtr := formatter.NewDeltaFormatter()
		diffString, err = fmtr.Format(diff)
		if err != nil {
			// No error can occur
		}

	} else {

		fmt.Printf("[info]", "insert: Unknown Format %s\n", format)
		os.Exit(4)
	}

	println(diffString)
}

func (d *Diff) defineStore(store *storage.Store) {
	d.store = store
}

func (d *Diff) printJson(json string) {
	println(d.formatJson(json))
}

func (d *Diff) formatJson(body string) string {
	bodyBytes := []byte(body)
	var out bytes.Buffer
	err := json.Indent(&out, bodyBytes, "", "  ")
	if err != nil {
		println("Diff.formatJson: error formatting json")
	}
	return string(out.Bytes())
}

func (d *Diff) printHtml(body string) {
	println(body)
}
