package services

import (
	
	"github.com/elazarl/goproxy"
	"log"
	"net/http"
	"io/ioutil"
	"bytes"
	"strings"
	"time"
	"gitlab.com/amrodrigues/testcaseless/caseless/storage"
	"gitlab.com/amrodrigues/testcaseless/caseless"
)


// Start testcaseless in listening mode
// This creates a web proxy

type Listen struct {

	store *storage.Store
	app   string
	history *caseless.History
}


func (t *Listen) Listen(store storage.Store, history caseless.History) {

	// Start web proxy
	// ProcessRequest and ProcessResponse will intercept
	// any GET/POST request and responses

	prxy := goproxy.NewProxyHttpServer()
	prxy.Verbose = false

	prxy.OnRequest().DoFunc(t.ProcessRequest)
	prxy.OnResponse().DoFunc(t.ProcessResponse)

	println(" ")
	println("~~ Testcaseless is running in listening mode ~~")
	println("Your web client should use this HTTP service on port 8080 as a proxy for all requests.")

	log.Fatal(http.ListenAndServe(":8080", prxy))
	t.history.Stop()
	println("[info]", "Testcaseless has stopped.")
}


func (t *Listen) ProcessRequest(req *http.Request, ctx *goproxy.ProxyCtx) (*http.Request, *http.Response) {

	println("Url: " + req.URL.Path)

	// Process "virtual" proxy urls
	// these are for the proxy to supply data to testcaseless.js, not the tested endpoint
	// /testcaseless/testcaseless.js
	// /testcaseless/app/APP/requests/TS
	// /testcaseless/app/APP/post/Body/TS
	// where TS is the db timeline and APP the registered app name in timeline db table
	// a request TS has the same response TS
	//todo


	//todo save non-crawlable files to db and track diffs

	// ignore non-crawlable files
	// for now, just detect a "." as part of the last URL component
	// later we could Process CSS, etc. to test for broken urls

	if caseless.IsFile(req.RequestURI) {
		return req, ctx.Resp
	}

	// mux request
	// this creates context between a get/post request and its respective response
	// the UUID is the identifier that points to nodes in a recorded path

	n := new(caseless.Node)

	n.Cookies = req.Header.Get("Cookie")
	n.Body = strings.Trim(t.GetRequestBody(req), "\r\n")
	n.Ts = time.Now().String()

	q := req.URL.Query()
	q.Add("testcaseless-Ts", n.Ts)
	req.URL.RawQuery = q.Encode()

	n.Url = req.URL.Path
	n.Re = "request"
	n.Method = strings.ToLower(req.Method)
	n.Bytes = []byte(n.Body)
	n.Flattener = new(caseless.Flattener)
	n.Flattener.Flatten(n.Body)

	t.history.Compare(n)

	return req, ctx.Resp
}


func (t *Listen) ProcessResponse(res *http.Response, ctx *goproxy.ProxyCtx) *http.Response {

	// if there are pending Node diffs to classify, do that first:

	for t.history.DiffContainsNodes() {
		t.history.DiffClassifyNext()
	}

	// A Node from a GET or POST request arrives
	// For this stage of the project, only Process json
	// in the future, we might add other content types

	if res == nil {
		return res
	}


	// --- handle status codes

	if res.StatusCode >= 500 {
		caseless.AsciiError()
		println("[error]", "URL " + res.Request.URL.Path + " returned status: " + res.Status)
		return res
	}

	if res.StatusCode == 404 {
		// in replay mode, this might be a Url that does not exist anymore
		//todo in replay mode, ask user if Url does not exist anymore
	}

	if res.StatusCode >= 300 {
		return res
	}

	if caseless.IsFile(res.Request.URL.Path) {
		//todo save files in C* and Diff them
		return res
	}

	// ---

	var responseBody = strings.Trim(t.GetResponseBody(res), " \r\n")

	if !caseless.IsCrawlable(res.Header.Get("Content-Type"), responseBody) {
		return res
	}

	n := new(caseless.Node)
	n.Cookies = ""
	n.Ts = res.Request.URL.Query().Get("testcaseless-Ts")
	n.Re = "response"
	n.Method = strings.ToLower(res.Request.Method)
	n.Url = res.Request.URL.Path
	n.Body = responseBody
	n.Bytes = []byte(n.Body)
	n.ContentType = caseless.RealContentType(res.Header.Get("Content-Type"), n.Body)
	n.Flattener = new(caseless.Flattener)
	n.Flattener.Flatten(n.Body)

	t.history.Compare(n)

	return res
}



func (t *Listen) GetResponseBody(r *http.Response) string {

	// extract a GET Node Body as a string

	readBody, err := ioutil.ReadAll(r.Body)

	if err != nil {
		println("[error]", "GetRequestBody: Error reading Node: ", err)
		return ""
	}
	r.Body.Close()
	r.Body = ioutil.NopCloser(bytes.NewReader(readBody))
	return string(readBody[:])
}


func (t *Listen) GetRequestBody(r *http.Request) string {

	// extract a POST request Body as a string

	readBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		println("[error]", "GetRequestBody: Error reading request: ", err)
		return ""
	}
	r.Body.Close()
	r.Body = ioutil.NopCloser(bytes.NewReader(readBody))
	return string(readBody[:])
}


func (t *Listen) Stop() {
	t.history.Stop()
}

