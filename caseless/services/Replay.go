package services

import (
	"net/http"
	"strings"
	"io/ioutil"
	"fmt"
	"time"
	"gitlab.com/amrodrigues/testcaseless/caseless/storage"
	"gitlab.com/amrodrigues/testcaseless/caseless"
)


// Start Testcaseless in replay mode
// This will replay historical nodes

type Replay struct {

	store   *storage.Store
	app     string
	history *caseless.History
	cookies []*http.Cookie
}

func (r *Replay) Replay () {

	r.cookies = []*http.Cookie{}

	r.store = new(storage.Store)
	r.store.Connect()

	r.history = new(caseless.History)
	r.history.SetStore(r.store)

	println(" ")
	println("~~ Testcaseless is running in non-interactive REPLAY mode ~~")

	r.Start()
}


// replay request-response history in sequence

func (r *Replay) Start() {

	var tsSequence, requests, responses = r.store.FetchTimeline()

	for ix := 0; ix < len(tsSequence) ; ix++ {

		var ts = tsSequence[ix]

		if requests[ts] != nil {
			r.Process(ts, requests[ts], responses[ts])
		} else {
			println("[error] sequence does not have both request and response:", ts)
		}
	}

	println("Replay ended.")
}


func (r *Replay) Process(ts string, req *caseless.Node, resp *caseless.Node) {

	// call Url and update Node with current Ts and Body
	var newRespBody = r.HttpRequest(req)

	if resp == nil {
		println("Replay: no record of response for this call:", req.Url)
		return
	}

	// skip any check if Body is the same as in stored response

	if r.history.IsEquivalent(newRespBody, resp.Body) {
		println("No changes in", req.Method, req.Url)
		return
	}

	// turn historical response into actual one

	resp.Body = newRespBody
	resp.Ts = time.Now().String()

	r.history.Compare(resp)
}



// an http client

func (r *Replay) HttpRequest(n *caseless.Node) string {

	println("")
	println("")
	println("+++++++++++++++++++++++++++++++++++++++++++++++++++")
	println("")
	fmt.Printf("---> REPLAY: %s: %s %s\n", n.Ts, n.Method, n.Url)


	if n.Re != "request" {
		println("[error] Node is not a request Node.")
		return ""
	}

	cl := &http.Client{}

	var req *http.Request
	var err error

	// add form post data to request if necessary

	if n.Method == "post" {

		postData := strings.NewReader(n.Body)

		req, err = http.NewRequest(strings.ToUpper(n.Method), "http://localhost:9000" + n.Url, postData)
		if err != nil {
			fmt.Printf("[error] creating http post request: %s \n", err)
			return ""
		}

		req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	} else if n.Method == "get" {

		req, err = http.NewRequest(strings.ToUpper(n.Method), "http://localhost:9000" + n.Url, nil)
		if err != nil {
			fmt.Printf("[error] creating http post request: %s \n", err)
			return ""
		}

	} else {

		println("Unsupported http Method", n.Method)
		return ""
	}


	// add Cookies to request

	if r.cookies != nil {

		for i := 0 ; i < len(r.cookies); i++ {
			println("HTTP request: Adding cookie:", r.cookies[i].String())
			req.AddCookie(r.cookies[i])
		}
	}


	resp, err := cl.Do(req)

	if err != nil {
		fmt.Printf("[error] getting http response: %s \n", err)
		return ""
	}

	// save received Cookies for next requests

	cookies := resp.Cookies()

	if len(cookies) > 0 {
		r.cookies = cookies
		println("--")
		println("Got Cookies:")
		for i := 0; i < len(cookies); i++ {
			println(cookies[i].String())
		}
		println("--")
	}


	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("[error] getting data: %s \n", err)
		return ""
	}

	return string(data)
}

