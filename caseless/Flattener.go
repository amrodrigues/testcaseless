package caseless

import (
	"strings"
	"github.com/src/github.com/json-iterator/go"
	"fmt"
	"errors"
	"reflect"
)

/*

   Takes a json string and flattens json values (path --> value | range | pattern | set)

   This allows to compare JSON values with their history.

   Adapted from: https://github.com/chandler767/JSON-Path-Flattener/blob/master/main.go

   JSON values may be replaced by:

   - A regex pattern (e.g. a date YYYY-MM-DD pattern)
   - A range (e.g. 1-5)
   - A set (e.g. 10,20,30 or "yes", "no")

   This is set as part of user input and is saved on DB.

*/


// a path to value mapping

type PathValue struct {
	path string
	value string
}


type Flattener struct {
	err           error
	nestedjson    map[string]interface{}
	flattenedjson map[string]string
}



// Turn a JSON body into a map of flattened paths to values --> values

func (f *Flattener) Flatten (jsonToFlatten string) {

	// make sure there are no newlines in input:

	jsonToFlatten = strings.Replace(jsonToFlatten, "\n", "", -1)

	f.flattenedjson = make(map[string]string)

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	err := json.Unmarshal([]byte(jsonToFlatten), &f.nestedjson)

	if err != nil {
		fmt.Println(errors.New("could not unmarshal JSON. A valid path, URL, or string is required"))
		panic(err)
	}

	for key, val := range f.nestedjson {
		f.process(f.flattenedjson, key, reflect.ValueOf(val))
	}

	pathValues := make([]PathValue, len(f.flattenedjson))

	for key, val := range f.flattenedjson {

		pathValues = append(pathValues, PathValue{key, val})
		fmt.Println(fmt.Sprintf(".%s=%s", key, val))
	}

	return
}


// De-serialize from DB
// FlatBody is saved in db because often values are converted to patterns, ranges or lists after user input
// This will take the DB string value and explodes it into the local map

func (f *Flattener) Load (flatBody string) {

	// Convert to slice:

	lines := strings.Split(flatBody, "\n")

	// Each line is separated by the string @testcaseless@
	// Split KEY@testcasseless@VALUE

	for ix := range lines {
		kv := strings.Split(lines[ix], "@testcaseless@")
		f.flattenedjson[kv[0]] = kv[1]
	}

}


// get flattened map as string, this is for db storage

func (f *Flattener) AsString() string {

	s := ""
	for k, v := range f.flattenedjson {
		s = s + k + "@testcaseless@" + v + "\n"
	}
	return s
}


// return map of path --> value

func (f *Flattener) AsMap() map[string]string {
    return f.flattenedjson
}


// add a path --> value to the current map

func (f *Flattener) Add(path string, value string) {

	if f.flattenedjson == nil {
		f.flattenedjson = make(map[string]string)
	}

	f.flattenedjson[path] = value
}

func (f *Flattener) Exists(path string) bool {
	if _, ok := f.flattenedjson[path]; ok {
		return true
	}
	return false
}

func (f *Flattener) process(jsontoflatten map[string]string, prefix string, val reflect.Value) {

	if val.Kind() == reflect.Interface {
		val = val.Elem()
	}

	switch val.Kind() {

	case reflect.Bool:

		if val.Bool() {
			jsontoflatten[prefix] = "true"
		} else {
			jsontoflatten[prefix] = "false"
		}

	case reflect.Int:

		jsontoflatten[prefix] = fmt.Sprintf("%d", val.Int())

	case reflect.Float64:

		jsontoflatten[prefix] = strings.TrimRight(strings.TrimRight(fmt.Sprintf("%.2f", val), "0"), ".") // Remove trailing zeros.

	case reflect.Map:

		for _, key := range val.MapKeys() {

			if key.Kind() == reflect.Interface {
				key = key.Elem()
			}
			if key.Kind() != reflect.String {
				panic(fmt.Sprintf("Key is not string: %s", key))
			}
			f.process(jsontoflatten, fmt.Sprintf("%s.%s", prefix, key.String()), val.MapIndex(key))
		}

	case reflect.Slice:

		for i := 0; i < val.Len(); i++ {
			f.process(jsontoflatten, fmt.Sprintf("%s[%d]", prefix, i), val.Index(i))
		}

	case reflect.String:

		jsontoflatten[prefix] = val.String()

	default:

		if !val.IsValid() {
			jsontoflatten[prefix] = "nil" // Insert a nil as string.
		} else {
			panic(fmt.Sprintf("Unexpected JSON value: %s", val)) // Check JSON format.
		}
	}
}

