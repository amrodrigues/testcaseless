package storage

import (
	"github.com/gocql/gocql"
	"log"
	"fmt"
	"os"
	"gitlab.com/amrodrigues/testcaseless/caseless"
)


// Handles Cassandra storage
// FlatBody is saved in db because often values are converted to patterns, ranges or lists after user input

type Store struct {
	cluster *gocql.ClusterConfig
	session *gocql.Session
}


// Connect to Cassandra

func (st *Store) Connect() {

	// Initial Cassandra connection
	// Executed only once per app lifetime

	st.cluster = gocql.NewCluster("127.0.0.1")
	st.cluster.ProtoVersion = 3
	st.cluster.Keyspace = "testcaseless"

	var err error
	st.session, err = st.cluster.CreateSession()
	if err != nil {
		fmt.Println("[error]", "Cassandra is not running. flatten conf/createdb.bash for instructions.")
		os.Exit(1)
	}
}


// Get a single node from db
// FlatBody contains a map of json_path --> value | pattern

func (st *Store) Fetch (uuid string, method string, requestOrResponse string) *caseless.Node {

	n := new(caseless.Node)
	n.Method = method

	var flatBody string

	q := st.session.Query("SELECT Url, Ts, Body, FlatBody, Cookies FROM " + method + requestOrResponse + "s WHERE uuid=? limit 1", uuid).Scan(&n.Url, &n.Ts, &n.Body, flatBody, &n.Cookies)

	if q.Error() != "" {
		log.Fatal(q.Error())
	}

	// Explode FlatBody:

	n.Flattener = new(caseless.Flattener)
	n.Flattener.Flatten(flatBody)

	return n
}


// sequence of request-responses for discovery / testing
// FlatBody contains a map of json_path --> value | pattern


func (st *Store) FetchTraversalHistory() []caseless.Node {

	var history = make([]caseless.Node, 0)
	iter := st.session.Query("SELECT Method, Ts, Re FROM timeline where app='default';").Iter()

	var uuid, method, ts, re, cookies string

	for iter.Scan(&uuid, &method, &ts, &re, &cookies) {

		var n = new(caseless.Node)

		n.Method = method
		n.Ts = ts
		n.Re = re
		n.Cookies = cookies

		history = append(history, *n)
	}

	println("Timeline: found ", len(history), " items...")

	return history
}



// timeline sequence of timestamps, a map with requests and a map with responses

func (st *Store) FetchTimeline() ([]string, map[string]*caseless.Node, map[string]*caseless.Node) {

	var sequence  = make([]string, 0)
	var requests  = make(map[string]*caseless.Node, 0)
	var responses = make(map[string]*caseless.Node, 0)

	iter := st.session.Query("select Ts,Re,Method,Url,Cookies,Body,FlatBody from timeline where app='default';").Iter()

	var ts, re, method, url, cookies, body, flatBody string

	for iter.Scan(&ts, &re, &method, &url, &cookies, &body, &flatBody) {

		var n = new(caseless.Node)

		n.Method = re
		n.Ts = ts
		n.Re = re
		n.Method = method
		n.Cookies = cookies
		n.Url = url
		n.Body = body

		// Explode FlatBody:

		n.Flattener = new(caseless.Flattener)
		n.Flattener.Flatten(flatBody)

		sequence = append(sequence, n.Ts)

		if n.Re == "request" {
			requests[ts] = n

		} else if n.Re == "response" {
			responses[ts] = n

		} else {
			println("[error] unknown value '" + re + "' in 'Re' column, should be 'request' or response'")
		}

	}

	println("Timeline: found", len(sequence), "items,", len(requests), "requests and", len(responses), "responses.")

	return sequence, requests, responses
}


// get history of a node

func (st *Store) FetchHistory(n *caseless.Node) []caseless.Node {

	if n == nil {
		println("[error] Store.FetchHistory: Node is nil")
	}

	var iter = st.session.Query("SELECT Url, Body, flatBody, Ts, Change, Cookies FROM "+n.Method+n.Re+"s WHERE Url=?;", n.Url).Iter()
	var url, body, flatBody, ts, change, cookies string
	var limit = 100

	var history = make([]caseless.Node, 0)

	for iter.Scan(&url, &body, &flatBody, &ts, &change, &cookies) {

		var r = new(caseless.Node)

		r.Url = n.Url
		r.Method = n.Method
		r.Ts = ts
		r.Body = body
		r.Change = change
		r.Bytes = []byte(body)
		r.Cookies = cookies

		// Explode FlatBody:

		n.Flattener = new(caseless.Flattener)
		n.Flattener.Flatten(flatBody)

		history = append(history, *r)

		if limit < 1 {
			break
		}

		limit--
	}

	println("History: found", len(history), "items...")

	return history
}


// save node in the database

func (st *Store) Insert(n *caseless.Node) bool {


	cql := "INSERT INTO "+n.Method +n.Re +"s (Url, Body, flatBody, Ts, Change, Cookies) VALUES (?, ?, ?, ?, ?, ?);"

	err := st.session.Query(cql, n.Url, n.Body, n.Flattener.AsString(), n.Ts, n.Change, n.Cookies).Exec()

	if err != nil {
		fmt.Println("[error]", "store.Insert: ", err)
		return false
	}


	// timeline

	cql = "INSERT INTO timeline (Method, Re, app, Ts, Url, Cookies, Body, flatBody) VALUES (?, ?, 'default', ?, ?, ?, ?, ?);"

	err2 := st.session.Query(cql, n.Method, n.Re, "default", n.Ts, n.Url, n.Cookies, n.Body, n.Flattener.AsString()).Exec()

	if err2 != nil {
		fmt.Println("[error]", "Insert into timeline: ", err2)
		return false
	}

	return true
}


// delete a node from db

func (st *Store) Delete(n *caseless.Node) bool {

	// Delete requests

	query := "DELETE FROM "+n.Method +"requests WHERE Url=? AND Ts=?;"
	err := st.session.Query(query, n.Url, n.Ts).Exec()

	if err != nil {
		fmt.Println("[error]", "store.Delete: ", err)
		return false
	}

	// Delete responses

	query = "DELETE FROM "+n.Method +"responses WHERE Url=? AND Ts=?;"
	err2 := st.session.Query(query, n.Url, n.Ts).Exec()

	if err2 != nil {
		fmt.Println("[error]", "store.Delete: ", err2)
		return false
	}

	query = "DELETE FROM timeline WHERE app='default' and Ts=?;"
	err3 := st.session.Query(query, n.Ts).Exec()

	if err3 != nil {
		fmt.Println("[error]", "store.Delete: ", err3)
		return false
	}

	return true
}


// delete all history of a node

func (st *Store) DeleteUrlHistory(n *caseless.Node) bool {

	// --- Delete Url requests

	query := "DELETE FROM "+n.Method +"requests WHERE Url=?;"
	err := st.session.Query(query, n.Url).Exec()

	if err != nil {
		fmt.Println("[error]", "store.DeleteUrlHistory: ", err)
		return false
	}

	// --- Delete Url responses

	query = "DELETE FROM "+n.Method +"responses WHERE Url=?;"
	err2 := st.session.Query(query, n.Url).Exec()

	if err2 != nil {
		fmt.Println("[error]", "store.DeleteUrlHistory: ", err2)
		return false
	}

	// --- Delete timeline entries for this Url

	var tsSequence, requests, responses = st.FetchTimeline()

	for ix := 0; ix < len(tsSequence) ; ix++ {
		var ts = tsSequence[ix]
		if requests[ts] != nil && (requests[ts].Url == n.Url || responses[ts].Url == n.Url) {
			st.DeleteTimelineEntry(ts)
		}
	}

	return true
}



// delete a single entry from timeline by timestamp

func (st *Store) DeleteTimelineEntry(ts string) {

	query := "DELETE FROM timeline WHERE app='default' and Ts=?;"
	err := st.session.Query(query, ts).Exec()

	if err != nil {
		fmt.Println("[error]", "store.DeleteTimelineEntry: ", err)
	}
}

