package caseless

import "strings"


// content analysis functions

func IsCrawlable(httpContentType string, body string) bool {

	return strings.Contains(httpContentType, "text/") ||

		IsJson(httpContentType, body) ||
		IsHtml(httpContentType, body)
}

func IsJson(httpContentType string, body string) bool {

	// some json payloads have text/plain as content type
	return strings.Contains(httpContentType, "application/json") || strings.Index(body, "{") == 0
}

func IsHtml(httpContentType string, body string) bool {
	return strings.Contains(httpContentType, "text/html") || strings.Contains(body, "<html")
}

func RealContentType(httpContentType string,  body string) string {

	if IsJson(httpContentType, body) {
		return "json"
	}

	if IsHtml(httpContentType, body) {
		return "html"
	}

	if IsFile(body) {
		return "file"
	}

	return ""
}

func IsFile(url string) bool {

	//todo analyse content type, not Url

	if url == "/" || url == "" {
		return false
	}

	var split = strings.Split(url, "/")
	if strings.Index(split[len(split)-1], ".") > -1 {
		return true
	} else {
		return false
	}
}


func AsciiError() {

	println("_____")
	println("|  ___|")
	println("| |__ _ __ _ __ ___  _ __")
	println("|  __| '__| '__/ _ \\| '__|")
	println("| |__| |  | | | (_) | |")
	println("\\____/_|  |_|  \\___/|_|")

}