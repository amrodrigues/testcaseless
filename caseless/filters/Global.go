package filters

import (
	"gitlab.com/amrodrigues/testcaseless/caseless"
	"strings"
	"regexp"
)


// Apply global filtering to all JSON values before scanning
// This will standardize values before scanning

type Global struct {

}

func (g* Global) Filter(node *caseless.Node) {


	// case-insensitive comparison
	// This avoids capitalisation style changes to be considered as variants
	s := strings.ToLower(node.FilteredBody)

	// replace values that trigger variants

	// YYYY-MM-DD dates
	var yymmdd = regexp.MustCompile(`[0-9]{4}-[0-9]{2}-[0-9]{2}`)
	s = yymmdd.ReplaceAllString(s, `2017-01-01`)

	// time
	var hhmm = regexp.MustCompile(`[0-9]{2}:[0-9]{2}`)
	s = hhmm.ReplaceAllString(s, `09:30`)

	// UUIDs
	var uuid = regexp.MustCompile(`[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4,10}`)
	s = uuid.ReplaceAllString(s, `00000000-0000-0000-0000-0000000000`)

	// pretty dates
	var prettyDates = regexp.MustCompile(`[0-9]?[0-9] [A-Z][a-z]{2}, 20[0-9][0-9]`)
	s = prettyDates.ReplaceAllString(s, `01 Jan, 2017`)

	// timestamps
	var timestamps = regexp.MustCompile(`[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9:\\.]+`)
	s = timestamps.ReplaceAllString(s, `2017-01-01 00:00:00.00000000`)

	// long numbers
	var longno = regexp.MustCompile(`[0-9]{8,20}`)
	s = longno.ReplaceAllString(s, `00000000`)

	node.FilteredBody = s
}

