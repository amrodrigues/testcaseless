package scanners

import "gitlab.com/amrodrigues/testcaseless/caseless"


// Json fixed value analysis
// Flags differences in Json values by comparing them to their history
// Eventually this will construct an acceptable range of values, if applicable
// node.flatBody should store testcaseless patterns when classified by the user, to be picked up by PatternMatching

type ExactValue struct {}


// return true if an exact historical match is found

func (s *ExactValue) Run (n *caseless.Node, history []*caseless.Node) bool {

	if len(history) == 0 {
		return false
	}

    if !s.findExact(n,history) {
    	return false
	}

	return true
}


// browse history and find an exact match in structure and values:
// this is good for ignoring the order of key-values in the serialised json

func (s *ExactValue) findExact(n *caseless.Node, history []*caseless.Node) bool {

	paths := n.Flattener.AsMap()

	// attempt to find the exact same node in history:

	for _, historyNode := range history {

		historyPaths := historyNode.Flattener.AsMap()

		// if there's any difference, cancel:

		equal := true

		for k, v := range paths {

			if v != historyPaths[k] {
				equal = false
				break
			}
		}

		if equal {
			return true
		}
	}

	return false
}

