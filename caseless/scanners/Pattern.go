package scanners

import "gitlab.com/amrodrigues/testcaseless/caseless"


// Json value pattern matching
// Flags differences in Json values based on stored value patterns

type PatternMatching struct {}


func (pm *PatternMatching) Run (n *caseless.Node, history []caseless.Node) bool {

	if len(history) == 0 {
		return false
	}

	paths := n.Flattener.AsMap()

	// process patterns if they exist

	for _, historyNode := range history {

		historyPaths := historyNode.Flattener.AsMap()

		// if there's any difference, cancel:
//todo
		equal := true

		for k, v := range paths {

			if v != historyPaths[k] {
				equal = false
				break
			}
		}

		if equal {
			return true
		}
	}

	return false

}