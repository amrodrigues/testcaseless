package scanners

import "gitlab.com/amrodrigues/testcaseless/caseless"
import "github.com/quii/jsonequaliser"


// Json structural analysis
// Flags differences in Json structure
// Flags differences in value types

type StructuralCompatibility struct {}


func (s *StructuralCompatibility) Run (n *caseless.Node, history []caseless.Node) bool {

	if len(history) == 0 {
		return false
	}

	for ix := 0; ix < len(history) ; ix++ {

		errors, err := jsonequaliser.IsCompatible(n.FilteredBody, history[ix].FilteredBody)

		if err != nil {

			print("ERROR: decoding json")
			continue
		}

		if len(errors) > 0 {

			print("ERROR:?")
			s.PrintErrors(errors)
			return false
		}

	}

	return true
}


func (s *StructuralCompatibility) PrintErrors (errors map[string]string) {

	for k := range errors {
		println(errors[k])
	}
}